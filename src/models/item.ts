import {Schema, Types, model, Model } from "mongoose";
import { Persons } from "../interfaces/persons.intercace";


const ItemSchema = new Schema<Persons>(
    {
        nombre:{
            type: String,
        },
        apellido:{
            type: String,
        },
        email:{
            type: String,
        },
        edad:{
            type: Number,
        }

    },
    {
        versionKey: false,
        timestamps: true,
    }
)

const ItemModel = model('items', ItemSchema);
export default ItemModel