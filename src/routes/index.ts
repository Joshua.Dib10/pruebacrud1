import {Router} from "express";
import {readdirSync} from "fs"

const PATH_ROUTER = __dirname
const router= Router();
//multiruta para varios routes en la carpeta
const cleanFileName = (fileName: string) => {
    const file = fileName.split(".").shift(); //shift devuelve el primer item de un array
    return file;
  };


//carga dinamicamente rutas 
readdirSync(PATH_ROUTER).filter((fileName) => {
    const cleanName = cleanFileName(fileName);
    if (cleanName !== "index") {
      import(`./${cleanName}`).then((moduleRouter) => {
        console.log(`Se esta cargado la ruta... /${cleanName}`)
        router.use(`/${cleanName}`, moduleRouter.router);
      });
    }
  });

export{router};