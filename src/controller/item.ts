import {Request, Response} from "express"
import { insertPersons, getPersons, getPerson, updatePerson, deletePerson} from "../service/item"

const getItem = async  ({params}: Request, res: Response) => {
    try{       
        const { id }= params;
        const response = await getPerson(id);
        const data = response ? response: "NO_EXISTE";
        console.log(data)
        res.send(data);        
        
    } catch(e){
        res.status(500);
        res.send("ERROR_GET_ITEM");
    }
}

const getItems = async (req: Request, res:Response) => {
    try{
        const response = await getPersons();
        res.send(response);
        console.log(response)
    } catch(e){
        res.status(500);
        res.send("ERROR_GET_ITEM");
    }
}

const updateItem = async ({params, body}: Request, res:Response) => {
    try{
        const {id } = params;
        const response = await updatePerson(id, body);
        res.send(response);
    } catch(e){
        res.status(500);
        res.send("ERROR_UPDATE_ITEM");
    }
}
const postItem = async ({ body }: Request, res: Response) => {
    try{
        const responseItem = await insertPersons(body)
        res.send(responseItem);
    } catch(e){
        res.status(500);
        res.send("ERROR_POST_ITEM");
    }
}
const deleteItem = async ({params}:Request, res:Response) => {
    try{
        const { id } = params;
        const response = await deletePerson(id);
        res.send(response);
    } catch(e){
        res.status(500);
        res.send("ERROR_DELETE_ITEM");
    }
}

export {getItems,getItem,updateItem,postItem,deleteItem};