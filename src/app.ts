import "dotenv/config"
import express from "express";
import cors from "cors";
import mongoose  from "mongoose";
import dbConnect from "./config/mongo";
import { router } from "./routes"

const PORT = process.env.PORT || 3001;


const app = express();
app.use(cors());
app.use(express.json());
app.use(router)

 


dbConnect().then(() => console.log("conexion lista"));
app.listen(PORT, () => console.log('App lista en puerto:', {PORT}));


export default app;