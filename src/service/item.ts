import ItemModel from "../models/item"
import { Persons } from "../interfaces/persons.intercace";

const insertPersons = async (item: Persons) => {
    const responseInsert = await ItemModel.create(item);
    return responseInsert;
};

const getPersons = async () => {
    const responseItems = await ItemModel.find({});
    return responseItems;
}

const getPerson = async (id: string) => {
    const responseItems = await ItemModel.findOne({_id:id});
    console.log(responseItems)
    return responseItems;
}
const updatePerson = async (id: string, data: Persons) => {
    const responseItems = await ItemModel.findOneAndUpdate({_id:id}, data,
         {new: true,
        });
    return responseItems;
}
const deletePerson = async (id: string) => {
    const responseItems = await ItemModel.deleteOne({_id:id})
    return responseItems;
}

export {insertPersons, getPersons, getPerson, updatePerson, deletePerson}