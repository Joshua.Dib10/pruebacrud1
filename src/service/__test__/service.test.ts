import { getPersons, getPerson } from "../item";
import ItemModel from "../../models/item";


jest.mock('../../models/item.ts', () => ({
    create: jest.fn(),
    find: jest.fn(),
    findOne: jest.fn(),
    findOneAndUpdate: jest.fn(),
    deleteOne: jest.fn()
}))

describe('Test servicio', () => {
    it('obtener todass las personas', async () =>{

        const mockData= [
            {_id: '1', name: 'Joshua', age:12},
            {_id: '2', name: 'Jorge', age:18}
        ]
        console.log(mockData[0]);
        console.log(mockData[1]);

        (ItemModel.find as jest.Mock).mockResolvedValue(mockData);

        const a = await getPersons();
        console.log(a)

        expect(ItemModel.find).toHaveBeenCalled();
        expect(a).toEqual(mockData)     

    })

    it('obtener una persona', async () =>{

        const mockId= "123"
        const mockPerson= [
            {_id: mockId, name: 'Joshua', age:12}            
        ]
        console.log(mockPerson[0]);

        (ItemModel.findOne as jest.Mock).mockResolvedValue(mockPerson);

        const a = await getPerson(mockId);
        console.log(a)

        expect(ItemModel.findOne).toHaveBeenCalledWith({_id: mockId});
        expect(a).toEqual(mockPerson)     

    })


})