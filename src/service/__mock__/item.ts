const mockPerson={
    _id: "1",
    nombre: "joshua",
    apellido: "dib",
    email: "jdib@zeroq.cl",
    edad: 25
}

export const getPerson = jest.fn().mockResolvedValueOnce([mockPerson]);